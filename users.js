function fetchData(data) {
    return new Promise(resolve => setTimeout(() => {
      resolve(data)
    }, 1000))
}

function sendUserLoginRequest(userId){
    console.log(`Sending login request for ${userId}...`);
    return fetchData(userId).then(() => {
        console.log('Login successfull.', userId);
        return userId;
    })
}

function getUserProfile(userId){
    console.log(`Fetching profile for ${userId}...`);
    return fetchData({
        user1: {name: 'Vijay', points: 100},
        user2: {name: 'Sahana', points: 200}
    }).then(profiles => {
        console.log(`Received profile for ${userId}`);
        return profiles[userId];
    });
}

function getUserPosts(userId){
    console.log(`Fetching posts for ${userId}...`);
    return fetchData({
        user1: [{id: 1, title: 'Economics 101'}, {id: 2, title: 'How to negotiate'}],
        user2: [{id: 3, title: 'CSS Animations'}, {id: 4, title: 'Understanding event loop'}]
    }).then(posts => {
        console.log(`Received posts for ${userId}`);
        return posts[userId];
    });
}


/**
 * Task 1: Send a login request for user1 -> get user profile data -> get user posts data
 */

async function userDataSerial(user){
  console.time('userData-serial');
  // Write code here
    let userId  = await sendUserLoginRequest(user)
    let profile = await getUserProfile(userId)
    let post = await getUserPosts(userId)
  console.timeEnd('userData-serial');
  console.log({
    "userData": {
      userId,
      profile,
      post
    }
  });
}

/**
 * Task 2: Send a login request for user1 -> get user profile data and get user posts data simultaneously
 */

async function userDataParallel (user) {
  console.time('userData-parallel');
  // Write code here
  let userId = await sendUserLoginRequest(user);
  await Promise.all([getUserProfile(userId), getUserPosts(userId)])
    .then(result => {
      let profile = result[0];
      let post = result[1];
      console.log({
        "userData": {
          userId,
          profile,
          post
        }
      });
    })

  console.timeEnd('userData-parallel');
}

userDataSerial("user1");
userDataParallel("user1");