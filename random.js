function fetchRandomNumbers() {
  return new Promise((resolve, reject) => {
    console.log("Fetching number...");
    setTimeout(() => {
      let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
      console.log("Received random number:", randomNum);
      resolve(randomNum);
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
  });
}

function fetchRandomString() {
  return new Promise((resolve, reject) => {
    console.log("Fetching string...");
    setTimeout(() => {
      let result = "";
      let characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      let charactersLength = characters.length;
      for (let i = 0; i < 5; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }
      console.log("Received random string:", result);
      resolve(result);
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
  });
}

fetchRandomString(console.log);
fetchRandomNumbers(console.log);

function addRandomNumbers() {
  let sum = 0;
  fetchRandomNumbers()
  .then((num) => {
    sum += num;
    console.log(sum);
  })
  .then(() => {
    let num2 = fetchRandomNumbers();
    return num2;
  }).then((num2) => (sum += num2))
    .then(console.log);
}

addRandomNumbers();

let num = fetchRandomNumbers()
let str = fetchRandomString()
const result = Promise.all([num, str]);
result.then((arr) => arr[0] + arr[1]).then(console.log)

function addRandomNumbers() {
  let nums = [];
  for (let i = 0; i < 10; i++){
    nums.push(fetchRandomNumbers());
  }
    const promise = Promise.all(nums);
    promise.then(arr => arr.reduce((sum, n) => sum + n, 0))
        .then(console.log);
}

// Using async await

const addTwoRandomNumbers = async () => {
    let sum = 0;
    await fetchRandomNumbers().then(randomNumber=> sum+= randomNumber )
    console.log(sum)
    await fetchRandomNumbers().then(randomNumber => sum += randomNumber)
    console.log(sum)
}

const addRandomNumberAndString = async () => {
  let result = "";
  await fetchRandomNumbers().then((num) => (result = num));
  await fetchRandomString().then((str) => (result += str));
  console.log(result);
};

const addRandomNumbers = async (num) => {
  try {
    let arr = [];
    for (let i = 0; i < num; i++) {
      arr.push(fetchRandomNumbers())
    }
    let nums = await Promise.all(arr)
    let result = nums.reduce((sum, num) => sum += num, 0);
    console.log(result);
  } catch (e){
    console.error(e)
  }
};

addTwoRandomNumbers()
addRandomNumberAndString()
addRandomNumbers(10);
